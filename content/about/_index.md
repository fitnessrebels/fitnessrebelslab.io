---
title: "Om oss"
date: 2018-07-12T18:19:33+06:00
description : "Fitness-Rebels.com´s hela idé är att motivera, inspirera och väcka intresse hos så många som möjligt. Fysisk träning är en mänsklig rättighet och borde egentligen inte kosta någonting alls. Vårt mål är att våra kunder lär sig och börjar uppskatta träning så mycket att dem aldrig mer behöver våra tjänster, eller något gyms tjänster. Enligt de senaste ekonomiska rapporterna om gym i Europa, så har medlemskaps köpen ökat, samtidigt som medlemmarnas gymbesök har minskat. Det gillar inte vi, därför skapar vi en ny marknad med målet att få så många som möjligt att hålla sig i form utan dyra gym medlemskap. I dagens samhälle finns det fler utbildade PT´s, än betalande kunder. Vi vill ge så många som möjligt motivationen, kunskapen och viljan att hålla sig hälsosamma. Bli frisk och hälsosam, så får du en fitness kropp, som en välkommen biverkan."
---
title: "Personliga hälsocoacher"
date: 2018-07-12T18:19:33+06:00
description : "Då vi på Fitness-Rebels.com tänker helt tvärtemot andra PT´s etc, - vi gör dig frisk och hälsosam, sedan får vi dig i form. Så har vi valt att kalla oss för “personliga “hälsocoacher”. Vi ger personlig coaching, genom att informera och anpassa fysisk träning. Kost och kosttillskott, får ni i form av information till varje tjänst. Dieter fungerar inte, men att äta organisk mat, som vilt eller bär som ni plockat i skogen smakar inte bara bättre - det innehåller otroligt mycket mer näring dessutom! All information, är just information och inte rådgivning. Hur ni använder informationen är ert ansvar."
---

