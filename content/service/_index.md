---
title: "Tjänster"
date: 2018-07-14T12:58:14+06:00
description : "Vi har ett öppnings erbjudande, en rabatt på 25%, som gäller hela sommaren, Juni, Juli och Augusti. Träningen sker där ni önskar, utomhus, inomhus, hemma hos dig eller på ditt hotellrum. Vi tar med sig den träningsutrustning som behövs. Ni som redan har träningsutrustning, eller vill köra utan, erhåller 10% extrarabatt på samtliga tjänster, dvs 35% rabatt totalt under sommarrean. "
---

