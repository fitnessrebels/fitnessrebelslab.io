---
title: "Källmaterial"
date: 2019-04-08T18:19:33+06:00
description : "Vi på Fitness-Rebels håller oss uppdaterade gällande alla våra metoder. Därför uppdaterar och granskar vi källmaterialet regelbundet.
H.I.T & H.I.I.T Nedan följer studier, reviews och artiklar som klart visar på fördelen med high intesity interval training (H.I.I.T) kontra steady state cardio. Jag har valt ut ett fåtal av flera tusen studier."
type: references
---
# Källmaterial


Vi på Fitness-Rebels håller oss uppdaterade gällande alla våra metoder. Därför uppdaterar och granskar vi källmaterialet regelbundet.
H.I.T & H.I.I.T Nedan följer studier, reviews och artiklar som klart visar på fördelen med high intesity interval training (H.I.I.T) kontra steady state cardio. Jag har valt ut ett fåtal av flera tusen studier.

H.I.I.T (High intensity interval training);

https://www.ncbi.nlm.nih.gov/pubmed/28076352

https://www.ncbi.nlm.nih.gov/pubmed/28116314

https://www.ncbi.nlm.nih.gov/pubmed/27774458

https://www.ncbi.nlm.nih.gov/pubmed/28251399

https://www.ncbi.nlm.nih.gov/pubmed/28639441

https://www.ncbi.nlm.nih.gov/pubmed/27797726

H.I.T (High intensity training)
Här följer en artikel someskriver fördelarna med H.I.T

”High-Intensity Interval Resistance Training (HIRT) influences resting energy expenditure and respiratory ratio in non-dieting individuals.” (https://www.ncbi.nlm.nih.gov/pubmed/23176325)

”Our investigation showed an increase in basal metabolism after resistance exercise which we were also able to detect 22 hours after the training session. The increase of REE after TT was about 5% whilst after HIRT it was 23% and these data suggest an important effect on metabolism corresponding to 452 Kcal per day.

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3551736/

Denna är också väldigt tydlig;

”Conclusions: These results indicate that 9 months of resistance training significantly increased RMR ~5% on average, but there was wide variability between individuals, which can be partially accounted for by changes in Fat free mass and thyroid hormones.”

http://www.nature.com/ejcn/journal/v69/n7/full/ejcn2014216a.html?foxtrotcallbac k=true

Den är även publicerad i NCBI

https://www.ncbi.nlm.nih.gov/pubmed/25293431

UROLOGY-ANDROLOGY – JUNE 1985

Anthropomorphic, hormonal, and psychologic correlates of semen quality in endurance-trained male athletes

W.T.AyersM.D.†YukoKomesuM.D.TheresaRomaniВ.S.RudiAnsbacherM.D.

”From these data we conclude that (1) vigorous endurance training may be associated with significantly decreased Testoterone values”

http://www.sciencedirect.com/science/article/pii/S001502821648622X?via%3Dihub (https://www.ncbi.nlm.nih.gov/pubmed/3158553) MED SCI SPORTS EXERC. 1988 FEB;20(1):60-5. REPRODUCTIVE HORMONAL PROFILES OF ENDURANCE-TRAINED AND UNTRAINED MALES. Hackney AC1, Sinning WE, Bruot BC. ” The results suggested normal hypothalamic-pituitary function existed in the trained subjects, and prolactin and cortisol were not causative factors in the lowered resting testosterone and free testosterone levels. The findings indicate that chronic endurance training lowers testosterone and free testosterone in males possibly by impairing testicular function.”

https://www.ncbi.nlm.nih.gov/pubmed/3343919

Eur J Appl Physiol. 2005 Jan;93(4):375-80. Epub 2004 Nov 20. RELATIONSHIP BETWEEN STRESS HORMONES AND TESTOSTERONE WITH PROLONGED ENDURANCE EXERCISE.

”In conclusion, the present findings give credence to the hypothesis suggesting a linkage between the low resting testosterone found in endurance-trained runners and stress hormones, with respect to cortisol.” https://www.ncbi.nlm.nih.gov/pubmed/15618989 Eur J Intern Med. 2008 Dec;19(8):598-601. doi: 10.1016/j.ejim.2007.06.032. Epub 2008 Apr 11. HORMONAL RESPONSES TO MARATHON RUNNING IN NON-ELITE ATHLETES.

Karkoulias K1, Habeos I, Charokopos N, Tsiamita M, Mazarakis A, Pouli A, Spiropoulos K.

”BACKGROUND: Exercise is known to be a powerful stimulus for the endocrine system. The hormonal response to exercise is dependent on several factors including the intensity, duration, mode of exercise (endurance versus resistance), and training status of the subject. The aim of the present study was to determine the steroid hormonal response (immediately after a race and 1 week later) to endurance exercise under the real conditions of the classic Athens marathon in a group of well-trained, middle-aged, non-elite athletes. METHODS: Blood samples were drawn 1 week before the race, directly after completion of the race, and 1 week later. RESULTS: Serum cortisol and prolactin showed distinct rises 1 h after the race and returned to baseline 1 week later. Androstenedione and dehydroepiandrosterone sulphate did not show any changes. Total testosterone as well as free testosterone dropped significantly 1 h after the race but returned to baseline 1 week later. CONCLUSION: In this particular group of non-elite, middle-aged marathon runners, the race resulted in an acute increase in serum cortisol and prolactin levels and in a concomitant decline in testosterone level. The aforementioned changes returned to baseline 1 week later.”

https://www.ncbi.nlm.nih.gov/pubmed/19046725

Steady state cardio och varför jag absolut inte kan rekommendera det, varken för män eller kvinnor!

https://www.ncbi.nlm.nih.gov/pubmed/19753538

”CONCLUSIONS: Exhaustive exercise; (1) decreases select thyroid hormones by 24 hours into recovery, (2) cortisol responses are inversely related to these thyroid reductions, and (3) prolactin responses (increases) are directly related to TSH changes.”

https://www.ncbi.nlm.nih.gov/pubmed/19046725 Som tur är så tog det bara en vecka för kroppen att återhämta si.

“CONCLUSION: In this particular group of non-elite, middle-aged marathon runners, the race resulted in an acute increase in serum cortisol and prolactin levels and in a

concomitant decline in testosterone level. The aforementioned changes returned to baseline 1 week later.”

https://www.ncbi.nlm.nih.gov/pubmed/15618989

“In conclusion, the present findings give credence to the hypothesis suggesting a linkage between the low resting testosterone found in endurance-trained runners and stress hormones, with respect to cortisol.”

Prospective study of hormonal and semen profiles in marathon runners. https://www.ncbi.nlm.nih.gov/pubmed/7589675

Testosterone - T Progesterone - P Estradiol – E2 Prolactin - PRL Luteinizing Hormone - LH

Follicle Stimulating Hormone – FSH The intensity of training increased significantly in the first 5 months of the study. This was accompanied by a significant rise in serum PRL levels and a fall in P levels. No other significant hormonal changes were identified. The semen volume and sperm motility and morphology fell significantly during training, but there was no significant alteration in the sperm count. CONCLUSION: This longitudinal study demonstrates that endurance training can modify significantly hormonal profiles and semen parameters in long-distance runners

”Cross education”				
Snabbar upp läkeprocessen 2-4 gånger beroende på hur allvarlig skadan är, kosten påverkar självklart, hur pass mycket vila med kvalitet man erhåller, men det som verkligen överraskar dem flesta som prövar ”cross education” är gur snabbt man återhämtar sig!

Ett fenomen som ”cross education”, upptäcktes över 100 år sedan och används sedan dess, mest på patienter som haft en stroke.

En ”Case study” på mig själv
Jag slet loss min yttre vänstra biceps sena, November år 2017.

Läkaren ordinerade sjukgymnastik, inga kost-råd, tyckte att jag skulle sluta med styrketräning och skrev ut ett gäng mediciner på recept (mest paracetamol – en av dem farligaste ämnen du kan utsätta din lever för). Han visste att jag är insatt i styrketräning – sedan 14 års ålder. Med nästan 30 års erfarenhet, var hans prognos 6 månader.
Alltså han trodde att jag skulle börja träna på gym om drygt 6 månader.

Det tog mig drygt 2 veckor med hjälp av ”cross – education”. Själva träningen gjorde jag 3-5 gånger i veckan, och tog aldrig längre än 15-20 minuter.

Nedan följer kliniska studier, meta studier, case studies etc;


https://www.ncbi.nlm.nih.gov/pubmed/30874884

https://www.ncbi.nlm.nih.gov/pubmed/30874884

https://www.ncbi.nlm.nih.gov/pubmed/21924681

https://www.ncbi.nlm.nih.gov/pubmed/29668382

https://www.ncbi.nlm.nih.gov/pubmed/17932739

{{< youtube tF7pmUmyzTk >}}

{{< youtube HEVJmfvKnwE >}}

Brunt fett (brown adipose tssue) - något som till skillnad alstrar värme jömfört med vitt fett!

https://www.ncbi.nlm.nih.gov/m/pubmed/26227180/?i=10&from=brown%20adipose%20tissue%20bat%20burning

https://www.ncbi.nlm.nih.gov/m/pubmed/27438137/?i=8&from=brown%20adipose%20tissue%20bat%20burning

https://www.ncbi.nlm.nih.gov/m/pubmed/25351615/?i=15&from=brown%20adipose%20tissue%20bat%20burning

https://www.ncbi.nlm.nih.gov/m/pubmed/29514878/?i=2&from=brown%20adipose%20tissue%20bat%20burning

https://www.ncbi.nlm.nih.gov/m/pubmed/25434908/?i=14&from=brown%20adipose%20tissue%20bat%20burning

https://www.ncbi.nlm.nih.gov/m/pubmed/25351615/?i=15&from=brown%20adipose%20tissue%20bat%20burning

{{< youtube T795c2G9fqQ >}}

Taurine 3 grams
Glycin 3 grams
L-Tryptophan 1 gram
Magnesium trionate 150 mg
L- Theonin 200 mg
Lemon balm 500 mg
Ashwaganda 500 mg

Meal frequency

{{< youtube OlgxQ2ZkV-Q >}}

Kolesterol myten;

{{< youtube gP1NA5f4LfE >}}

{{< youtube JkZvFjW82Mk >}}

{{< youtube t_FRo_xbsOQ >}}

{{< youtube JipRwP754jA >}}

{{< youtube zx-QrilOoSM >}}

{{< youtube 3zX3tfuKIlo >}}

{{< youtube cYrXXmTR1e8 >}}

Hur socker industrin mutade forskare för att skylla på mättat fett istället för socker.

{{< youtube szOH5Aq2jYE >}}

{{< youtube apFQjNE6kFg >}}

{{< youtube QPOh3xq1uVQ >}}

{{< youtube apFQjNE6kFg >}}

{{< youtube szOH5Aq2jYE >}}
