---
title: "Friskrivningsklausul"
date: 2019-04-08T18:19:33+06:00
description : "För dom som blir en av våra kunder, är det ett frivilligt val som hålls konfidentiellt från vår sida.
Vi ger alltid information med källhänvisning med avsikt att optimera ditt välmående och/eller dina träningsprestationer."
type: friskrivningsklausul
---
# Detta är avsett som information

För dom som blir en av våra kunder, är det ett frivilligt val som hålls konfidentiellt från vår sida.

Vi ger alltid information med källhänvisning med avsikt att optimera ditt välmående och/eller dina träningsprestationer.

Vi vill dock uppmärksamma dig på följande: Fitness-Rebels.com har ingen kontroll över eventuella felkällor som kan påverka ditt välmående och/eller dina träningsprestationer. 

Människokroppen är en komplicerad organism som påverkas av ett antal olika faktorer, både interna och externa, och alla dessa ligger utanför vår kontroll. Med anledning av detta har vi inte möjlighet att ge några som helst garantier avseende resultat, ditt välmående och/eller dina träningsprestationer, även om du följer våra rekommendationer.

Den information som finns på vår hemsida utgör varken vård- och/eller medicinsk-rådgivning, och avser inte att diagnosticera, behandla, bota eller att förebygga skador eller/och sjukdomar.

Rekommendationerna och den information som finns på vår hemsida ska inte heller i något fall ersätta medicinsk rådgivning eller annan vård. Din läkare/vårdgivare bör du alltid fråga om råd angående läkemedel/kosttillskott och vård.
