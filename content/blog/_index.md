---
title: "Blog"
date: 2018-07-15T12:32:37+06:00
description : "Fitness-rebels.com vill inte in på fitness marknaden, vi vill skapa en ny marknad, till skillnad från alla andra fitness aktörer. Vi på Fitness-Rebels.com är nyfikna, passionerade och har ett brinnande intresse för allt inom fitness. Vi anser att fitness och träning varit alldeles för dyrt och vinstinriktat, och vill att alla ska ha råd med en egen PT, med slutmålet att göra så många som möjligt oberoende av gym och dyra PT´s."
---

