---
title: "Vlogg nummer 1"
date: 2019-04-29T12:29:40+06:00
description : "FITNESS-REBELS.COM presenterar vår första vlogg/blogg, som förklarar lite av våra ideer och bakgrund."
type: post
image: images/blog/20171128_113333387_iOS_4.jpg
author: Marcus Lundin
tags: ["motivation", "inspiration"]
---
FITNESS-REBELS.COM presenterar vår första vlogg/blogg, som förklarar lite av våra ideer och bakgrund.

Till skillnad från den övriga gymbranschen (nästan) så är vårt mål att lära så många som möjligt allt om näring och träning till så låga priser som möjligt. Alla som jobbar åt Fitness-Rebels.com är nyfikna, passionerade och följer världens forskning (den okorrumperade). Vi vill inte bara ge alla möjlighet att få en personlig tränare, men när vi lyckas lära upp en kund till den graden att han/hon inte behöver våra eller något annat gy i gym eller frilansande PT i Fitness branschen - ja, då har vi kommit ett steg närmare vårt mål! Nämligen att Fitness är en mänsklig rättighet som ej kan användas för att göra vinst, vi anser att man som brn borde få lära sig allt i skolan. I dagens samhälle säljs fler gym-medlemskap varje år, samtidigt som mindre och mindre kunder ej utnyttjar sitt gym-meldemskap. Detta är dessvärre något som går i linje med alla Europas gym ägare. Detta sk mål arbetar vi alla på Fitness-Rebels.com, emot med stolthet och helt och hållet utan skam.
{{< youtube 2c67897Rq0A >}}
