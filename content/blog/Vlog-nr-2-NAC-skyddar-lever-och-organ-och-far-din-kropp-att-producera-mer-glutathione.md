---
title: "Vlog nr 2 - N.A.C - skyddar lever och organ, och får din kropp att producera mer glutathione"
date: 2019-05-01T12:29:40+06:00
description : "N-acetylcystein återskapar leverfunktionen hos dem som lider av icke-alkoholisk fettlever."
type: post
image: images/IMG_0494.JPG
author: Marcus Lundin
tags: ["motivation", "inspiration"]
---

Jag gick ner till ca 88 kg från 125 -130 kg genom att äta mer fett  och mindre kolhydrater -fördelat på mycket färre måltider. Då jag tränade 8-10 ggr i vecka (ca 4 st 60 minuters löppass och ca 4 st styrketräningspass på ca 60 minuter - så minskade jag min träningsmängd med ca 75%.
.
Då gjorde jag det pga av tidsbrist och den största skillnaden var att jag började träna 4-5 H.I.T-pass (high intensity trainng) i veckan på ca 10-25 minuter (med TRX-band).

Min alkohol konsumtion minskades dessutom dramatiskt. från att dricka en gång i veckan till en gång varannan månad.

Sedan dess har jag läst allt jag kommit över om tröning och näringslära, och insett att nästan allt jag lärt mig om kost och träning är helt fel.

N.A.C(N-acetylcystein) 
N-acetylcystein återskapar leverfunktionen hos dem som lider av icke-alkoholisk fettlever.
Desto längre man medicinerar med N.A.C, desto bättre resultat uppnås.

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3270338/

https://www.ncbi.nlm.nih.gov/m/pubmed/9665023/

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2790590/

https://www.ncbi.nlm.nih.gov/m/pubmed/9305490/

https://youtu.be/8igqnKxz528

https://www.ncbi.nlm.nih.gov/m/pubmed/18806095/

https://www.ncbi.nlm.nih.gov/m/pubmed/28205121/

https://www.ncbi.nlm.nih.gov/m/pubmed/18161828/?i=2&from=/28205121/related

https://www.ncbi.nlm.nih.gov/m/pubmed/19524577/

https://www.ncbi.nlm.nih.gov/m/pubmed/22859317/

https://www.ncbi.nlm.nih.gov/m/pubmed/14633141/

https://www.ncbi.nlm.nih.gov/m/pubmed/27671340/

https://www.ncbi.nlm.nih.gov/m/pubmed/16221220/

https://www.ncbi.nlm.nih.gov/m/pubmed/12653105/

{{< youtube 8igqnKxz528 >}}
