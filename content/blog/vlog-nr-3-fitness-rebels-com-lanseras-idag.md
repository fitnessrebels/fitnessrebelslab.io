---
title: "Vlog nr 3 - Fitness-Rebels.com, lanseras idag!"
date: 2019-05-04T12:29:40+06:00
description : "I dagens samhälle finns det fler betalande kunder, än kunder som tränar på gymmet."
type: post
#image: images/IMG_0494.JPG
author: Marcus Lundin
tags: ["motivation", "inspiration"]
---
FITNESS-REBELS.COM
I dagens samhälle finns det fler betalande kunder, än kunder som tränar på gymmet. Vår uppgift är att ge så många som möjligt motivationen, kunskapen och viljan att hålla sig hälsosamma. Bli frisk och hälsosam, så får du en fitness kropp, som en välkommen biverkan.{{< youtube Z9JUnokq_VE >}}
